import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateBrandDto } from './dto/create-brand.dto';
import { UpdateBrandDto } from './dto/update-brand.dto';
import { v4 as uuid } from 'uuid';
import { Brand } from './interfaces';

@Injectable()
export class BrandsService {

  private _brands: Brand [] = [
    /*{
      id: uuid(),
      name: 'Toyota',
      createdAt: new Date().getTime()
    },*/
  ] 

  public create(createBrandDto: CreateBrandDto) {
    const instanceNewCar = this._buildInstance(createBrandDto)
    this._brands = [...this._brands, instanceNewCar];
    return instanceNewCar;
  }

  public findAll() {
    return this._brands;
  }

  public findOne(id: string) {
    const brandFound: Brand | null = this._brands.find(brand => brand.id === id);
    if(!brandFound){
      throw new NotFoundException(`Brand with id: ${id} not found`);
    }
    return brandFound;
  }

  public update(id: string, brandDto: UpdateBrandDto) {
    if(id !== brandDto.id) {
      throw new BadRequestException(`Brand with id: ${id} not same the body id`);
    }
    let brandFound = this.findOne(id);
    this._brands.map(brand => {
      if(brand.id === id){
        brandFound = { 
          ...brandFound,
          name: brandDto.name,
          updatedAt: new Date().getTime()
        }
        return brandFound;
      }
      return brand;
    })
    return brandFound;
  }

  public remove(id: string) {
    const brandFound = this.findOne(id);
    this._brands = this._brands.filter(brand => brand.id !== brandFound.id);
  }

  private _buildInstance(brandDto: CreateBrandDto): Brand {
    const instanceNewCar: Brand = new Brand();
    instanceNewCar.id = uuid();
    instanceNewCar.name = brandDto.name.toLocaleLowerCase();
    instanceNewCar.createdAt = new Date().getTime();
    return instanceNewCar;
  }

  public fillBrandsWithSeedData(brands: Brand[]) {
    this._brands = brands;
  }
}
