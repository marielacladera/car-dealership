import { IsBoolean, IsString, MinLength } from "class-validator";

export class CreateBrandDto {
    @IsString()
    @MinLength(1)
    name: string;

    @IsString()
    descriptionOne: string;

    @IsBoolean()
    isActiveOne: string;
}
