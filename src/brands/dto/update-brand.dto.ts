import { IsOptional, IsString, IsUUID, MinLength } from "class-validator";

export class UpdateBrandDto {
    @IsString()
    @IsUUID()
    @IsOptional()
    readonly id?: string;

    @IsString()
    @IsOptional()
    @MinLength(1)
    readonly name?: string;
}
