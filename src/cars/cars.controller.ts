import { Body, Controller, Delete, Get, Param, ParseIntPipe, ParseUUIDPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';

@Controller('cars')
export class CarsController {

    constructor(
        private readonly _carsService: CarsService
    ) {}
    
    @Get()
    public getAllCars() {
        return this._carsService.findAll()
    }

    @Get(':id')
    public getCarById(@Param('id', new ParseUUIDPipe({version: '4'})) id : string) {
        return this._carsService.findById(id);
    }

    @Post()
    public createCar(@Body() createCarDto: CreateCarDto) {
        return this._carsService.create(createCarDto);
    }
    
    @Patch(':id')
    public updateCar(@Param('id', ParseUUIDPipe) id:string, @Body() updateCarDto: UpdateCarDto){
        return this._carsService.update(id, updateCarDto);
    }

    @Delete(':id')
    public deleteCarById(@Param('id', ParseUUIDPipe) id : string) {
       return this._carsService.delete(id)
    }
}
