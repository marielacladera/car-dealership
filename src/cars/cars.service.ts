import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Car } from './interfaces/car.interface';
import { v4 as uuid} from 'uuid';
import { CreateCarDto, UpdateCarDto } from './dto';

@Injectable()
export class CarsService {
    private _cars: Car[] = [
        /*{
            id: uuid(),
            brand: 'Toyota',
            model: 'Corolla'
        },
        {
            id: uuid(),
            brand: 'Honda',
            model: 'Civic'
        },
        {
            id: uuid(),
            brand: 'Jeep',
            model: 'Cheverolet'
        },*/
    ]

    public create(createCarDto: CreateCarDto) {
        const instanceNewCar: Car = {
            id: uuid(),
            ...createCarDto
        };
        this._cars = [ ...this._cars, instanceNewCar];
        return instanceNewCar;
    }

    public findAll() {
        return this._cars;
    }

    public findById(id: string) {
        const car = this._cars.find(car => car.id === id);
        if(!car){
            throw new NotFoundException(`Car with id: ${id} not found`);
        }
        return car;
    }

    public update(id: string, updateCarDto: UpdateCarDto) {
        if(id !== updateCarDto.id) {
            throw new BadRequestException(`Car id ${id} not same the body id`);
        }
        let carDB = this.findById(id);
        this._cars = this._cars.map(car => {
            if(car.id === id) {
                carDB = { 
                    ...carDB, 
                    ...updateCarDto,
                    id
                }
                return carDB;
            }
            return car;
        })
        return carDB;
    }

    public delete(id: string) {
        const carDB = this.findById(id);
        this._cars = this._cars.filter(car => car.id === id);
    }

    
    public fillCarsWithSeedData(cars: Car[]) {
        this._cars = cars;
    }
}
