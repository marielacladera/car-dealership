import { Brand } from "src/brands/interfaces";
import { v4 as uuid } from "uuid";

export const BRANDS_SEED: Brand [] = [
    {
        id: uuid(),
        name: 'TOYOTA',
        createdAt: new Date().getTime()
    },
    {
        id: uuid(),
        name: 'Suzuki',
        createdAt: new Date().getTime()
    },
    {
        id: uuid(),
        name: 'Volvo',
        createdAt: new Date().getTime()
    },
    {
        id: uuid(),
        name: 'NISSAN',
        createdAt: new Date().getTime()
    }
]